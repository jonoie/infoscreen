﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NetworkCommsDotNet;
using NetworkCommsDotNet.Tools;
using NetworkCommsDotNet.DPSBase;
using NetworkCommsDotNet.Connections;
using NetworkCommsDotNet.Connections.TCP;

using System.Windows.Forms;
using NetworkData;
using NetworkData.Packets;
using System.IO;

namespace InfoScreenClient
{
    class Client
    {
        private int port = 2918;
        private ConnectionInfo connectionInfo { get; set; }

        public Client()
        {
            NetworkComms.DefaultSendReceiveOptions = new SendReceiveOptions(DPSManager.GetDataSerializer<ProtobufSerializer>(),
                NetworkComms.DefaultSendReceiveOptions.DataProcessors, NetworkComms.DefaultSendReceiveOptions.Options);

            Connect();
        }

        public void Connect()
        {
            NetworkComms.Shutdown();

            try
            {
                connectionInfo = new ConnectionInfo("192.168.1.110", port);
            }
            catch
            {
                MessageBox.Show("Unable to connect to server.");
                Environment.Exit(0);
            }
        }

        public void SendPacket(string packetType, object packetData)
        {
            try
            {
                TCPConnection.GetConnection(connectionInfo).SendObject(packetType, packetData);
            }
            catch (Exception err)
            {

                System.Diagnostics.Debug.WriteLine(err.ToString());
                NetworkComms.Shutdown();
            }
        }

        public void SendImage(System.Drawing.Bitmap image)
        {
            byte[] bImage; 

            using (var ms = new MemoryStream())
            {
                image.Save(ms, image.RawFormat);
                bImage = ms.ToArray();
            }

            Image sendImage = new Image(bImage);
            SendPacket(PacketNames.sendImage.ToString(), sendImage);
        }

        public void SendHideGUI()
        {
            SendPacket(PacketNames.hideGUI.ToString(), 1);
        }
    }
}
