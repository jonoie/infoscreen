﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace InfoScreenClient
{
    public partial class frmMain : Form
    {
        private Client client;

        public frmMain()
        {
            InitializeComponent();
            client = new Client();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult result = fileDialog.ShowDialog();

            if (result == DialogResult.OK || result == DialogResult.Yes)
            {
                if (fileDialog.CheckFileExists)
                {
                    try
                    {
                        Image bmp = Bitmap.FromFile(fileDialog.FileName);

                        client.SendImage((Bitmap)bmp);

                        MessageBox.Show("Image sent to screen.");
                    }
                    catch
                    {
                        MessageBox.Show("Error sending image to screen.");
                    }
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            client.SendHideGUI();

            MessageBox.Show("Hide/Show GUI Sent.");
        }
    }
}
