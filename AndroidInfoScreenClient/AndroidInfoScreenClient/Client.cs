﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using NetworkCommsDotNet;
using NetworkCommsDotNet.Tools;
using NetworkCommsDotNet.DPSBase;
using NetworkCommsDotNet.Connections;
using NetworkCommsDotNet.Connections.TCP;

using NetworkData;
using System.IO;
using Android.Graphics;

namespace AndroidInfoScreenClient
{
    class Client
    {
        private int port = 2918;
        private ConnectionInfo connectionInfo { get; set; }

        public Client()
        {
            NetworkComms.DefaultSendReceiveOptions = new SendReceiveOptions(DPSManager.GetDataSerializer<NetworkCommsDotNet.DPSBase.ProtobufSerializer>(),
                NetworkComms.DefaultSendReceiveOptions.DataProcessors, NetworkComms.DefaultSendReceiveOptions.Options);
            
            Connect();
        }

        public void Connect()
        {
            NetworkComms.Shutdown();

            try
            {
                connectionInfo = new ConnectionInfo("192.168.1.110", port);
            }
            catch
            {
               // MessageBox.Show("Unable to connect to server.");
                //Environment.Exit(0);
            }
        }

        public bool SendPacket(string packetType, object packetData)
        {
            try
            {
               // if (connectionInfo.IsConnectable && connectionInfo.ConnectionState == ConnectionState.Established)
              //  {
                    TCPConnection.GetConnection(connectionInfo).SendObject(packetType, packetData);
                    return true;
              //  }
              //  else
              //  {
                //    return false;
               // }
            }
            catch (Exception err)
            {

                System.Diagnostics.Debug.WriteLine(err.ToString());
                NetworkComms.Shutdown();
                return false;
            }
        }

        public void SendImage(Bitmap bmp)
        {
            byte[] image;
            using (var stream = new MemoryStream())
            {
                bmp.Compress(Bitmap.CompressFormat.Png, 0, stream);
                image = stream.ToArray();
            }

            NetworkData.Packets.Image sendImage = new NetworkData.Packets.Image(image);
            SendPacket(PacketNames.sendImage.ToString(), sendImage);
        }

        public bool SendHideGUI()
        {
            return SendPacket(PacketNames.hideGUI.ToString(), 1);
        }
    }
}