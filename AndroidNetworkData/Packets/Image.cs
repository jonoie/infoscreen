﻿using ProtoBuf;
using System.IO;
using NetworkCommsDotNet.DPSBase;

namespace NetworkData.Packets
{
    [ProtoContract]
    public class Image : Serializer, IExplicitlySerialize
    {
        [ProtoMember(1)]
        public byte[] image { get; private set; }

        private Image() { }

        public Image(byte[] image)
        {
            this.image = image;
        }

        public void Serialize(Stream outputStream)
        {
            SerializeData(outputStream, image);
        }

        public void Deserialize(Stream inputStream)
        {
            image = DeserializeBytes(inputStream);
        }

        public static void Deserialize(Stream inputStream, out Image image)
        {
            image = new Image();
            image.Deserialize(inputStream);
        }
    }
}
