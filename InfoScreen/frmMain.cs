﻿using InfoScreen.Network;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Authentication;
using System.Text;
using System.Windows.Forms;

namespace InfoScreen
{
    public partial class frmMain : Form
    {
        private bool debug = false;

        private bool showGUI = true;
        public static frmMain WINDOW;


        private Server server;
        private Feed.FeedWorker feedWorker;

        private List<CustomRTB> jobFeeds = new List<CustomRTB>();
        private List<CustomRTB> events = new List<CustomRTB>();

        private int fontSizeData = 11;
        private int fontSizeHeader = 13;

        private DateTime lastFeedDate;
        private DateTime lastWeatherDate;

        Calendar calendar;
        Weather weather;

        public frmMain()
        {
            WINDOW = this;

            const SslProtocols _Tls12 = (SslProtocols)0x00000C00;
            const SecurityProtocolType Tls12 = (SecurityProtocolType)_Tls12;
            ServicePointManager.SecurityProtocol = Tls12;

            InitializeComponent();

            lblTime.BackColor = Color.FromArgb(0, 10, 10, 10);
            lblTime.ForeColor = Color.FromArgb(200, 255, 255, 255);

            lblTemperature.BackColor = Color.FromArgb(0, 10, 10, 10);
            lblTemperature.ForeColor = Color.FromArgb(175, 255, 255, 255);

            lblCalendar.BackColor = Color.FromArgb(0, 10, 10, 10);
            lblFeed.BackColor = Color.FromArgb(0, 10, 10, 10);

            imgWeather.BackColor = Color.FromArgb(0, 10, 10, 10);

            calendar = new Calendar();
            weather = new Weather();

            lastFeedDate = DateTime.Now;
            lastWeatherDate = DateTime.Now;
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            server = new Server();
            feedWorker = new Feed.FeedWorker();

            if (debug)
            {
                this.WindowState = FormWindowState.Normal;
                this.FormBorderStyle = FormBorderStyle.SizableToolWindow;
            }

            CreateEventBoxes();
            CreateFeedBoxes();
            ResetFeedBoxes(true);

            UpdateLayout();

            PlaceCornerImage();

            UpdateDummyEvents();
            UpdateDummyFeed();

            firstRunTimer.Enabled = true;
        }

        public bool TimeLimitHit(DateTime time, int minutes)
        {
            DateTime dNow = DateTime.Now;

            int minuteDifferences = (int)(dNow - time).TotalMinutes;

            if (minuteDifferences >= minutes)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void UpdateWeatherBox()
        {
            int width = (int)(828 * 1f);
            int height = (int)(272 * 1f);

            imgWeather.Width = width;
            imgWeather.Height = height;

            Point lblCenter = GetCenterPosition(lblTime.Size);
            lblCenter.Y += lblTime.Size.Height;

            int lowerCenter = this.Height - lblCenter.Y;

            imgWeather.Location = new Point((this.Width / 2) - (imgWeather.Width / 2), lblCenter.Y + (lowerCenter / 2) - (imgWeather.Height / 2));

            Image image = weather.GetWeatherBitmap();

            if (image.Width != 2)
            {
                imgWeather.BackgroundImage = image;

                lblTemperature.Text = weather.GetTemperature().ToString() + " °C";
                lblTemperature.Location = new Point((this.Width / 2) - (lblTemperature.Width / 2), 150);
            }


            //    imgWeather.SizeMode = PictureBoxSizeMode.Zoom;
        }

        public void CreateFeedBoxes()
        {
            int heightPosition = 0;

            lblFeed.Location = new Point(12, 12);

            panelFeed.Size = new Size(this.Width / 2, this.Height - 30);
            panelFeed.Location = new Point(12, lblFeed.Height + 26);

            for (int i = 0; i < feedWorker.jobs.Count * 2; i++)
            {
                CustomRTB textBox = new CustomRTB();
                textBox.Location = new Point(0, heightPosition);

                if (IsOdd(i))
                {
                    textBox.Font = new Font("MonoSpace", fontSizeData, FontStyle.Regular);
                }
                else
                {
                    textBox.Font = new Font("MonoSpace", fontSizeHeader, FontStyle.Bold);
                }

                textBox.BorderStyle = BorderStyle.None;
                textBox.WordWrap = false;
                textBox.ScrollBars = RichTextBoxScrollBars.None;
                textBox.ReadOnly = true;
                textBox.Margin = new Padding(5);
                textBox.Enabled = false;

                panelFeed.Controls.Add(textBox);
                heightPosition += textBox.Height + 10;

                jobFeeds.Add(textBox);
            }
        }

        public void CreateEventBoxes()
        {
            int heightPosition = 0;

            lblCalendar.Location = new Point((this.Width) - lblCalendar.Width - 75, 12 + 240);

            panelEvents.Size = new Size((this.Width / 2) - 75, this.Height - lblCalendar.Height - 254);
            panelEvents.Location = new Point((this.Width / 2) + 75, 12 + 254 + lblCalendar.Height);

            for (int i = 0; i < (calendar.SingleEvent.Count) + (calendar.RecurringEvents.Count); i++)
            {
                CustomRTB textBox = new CustomRTB();
                textBox.Location = new Point(panelEvents.Width - 400 - 50 - 14, heightPosition);

                textBox.Font = new Font("MonoSpace", fontSizeHeader, FontStyle.Bold);

                textBox.BorderStyle = BorderStyle.None;
                textBox.WordWrap = false;
                textBox.ScrollBars = RichTextBoxScrollBars.None;
                textBox.ReadOnly = true;
                textBox.Margin = new Padding(5);
                textBox.Enabled = false;

                panelEvents.Controls.Add(textBox);
                heightPosition += textBox.Height + 10;

                events.Add(textBox);
            }
        }

        public bool IsOdd(int value)
        {
            return value % 2 != 0;
        }

        public void ResizeFeedBoxes()
        {
            int heightPosition = 0;

            for (int i = 0; i < feedWorker.jobs.Count; i++)
            {
                CustomRTB textHeader = jobFeeds[i * 2];
                CustomRTB textData = jobFeeds[i * 2 + 1];
                
                if (feedWorker.jobs[i].error == false && (bool)textHeader.Tag == true && (bool)textData.Tag == true)
                {
                    textHeader.Visible = true;
                    textData.Visible = true;

                    textHeader.Show();
                    textData.Show();

                    textHeader.Location = new Point(0, heightPosition);
                    textHeader.Size = new Size(400, textHeader.Height);
                    heightPosition += textHeader.Height;

                    textData.Location = new Point(0, heightPosition);
                    textData.Size = new Size(400, textData.Height);
                    heightPosition += textData.Height + 10;
                }
                else
                {
                    textHeader.Visible = false;
                    textData.Visible = false;

                    textHeader.Hide();
                    textData.Hide();
                }
            }
        }

        public void ResizeEventBoxes()
        {
            int heightPosition = 0;
            int currentIndex = 0;

            for (int i = 0; i < calendar.RecurringEvents.Count; i++)
            {
                CustomRTB textEvent = events[i];

                textEvent.Visible = true;

                textEvent.Location = new Point(textEvent.Location.X, heightPosition);
                textEvent.Size = new Size(400, textEvent.Height);
                heightPosition += textEvent.Height + 10;

                currentIndex += 1;
            }

            for (int i = 0; i < calendar.SingleEvent.Count; i++)
            {
                CustomRTB textEvent = events[currentIndex + i];

                textEvent.Visible = true;

                textEvent.Location = new Point(textEvent.Location.X, heightPosition);
                textEvent.Size = new Size(400, textEvent.Height);
                heightPosition += textEvent.Height + 10;
            }
        }

        public void UpdateEvents()
        {
            int currentIndex = 0;

            DateTime dateNow = DateTime.Now;
            for (int i = 0; i < calendar.RecurringEvents.Count; i++)
            {
                CustomRTB textEvent = events[i];

                CalendarEvent calendarEvent = calendar.RecurringEvents[i];

                TimeSpan diff =  calendarEvent.Date - dateNow;

                if (calendarEvent.Date < dateNow && !calendarEvent.EventReached)
                {
                    Speak("Event starting " + calendarEvent.Description);
                    calendarEvent.EventReached = true;
                }

                if (calendarEvent.EventReached)
                {
                    textEvent.ForeColor = Color.Green;
                }
                else
                {
                    textEvent.ForeColor = Color.DarkGray;
                }

                textEvent.Text = 
                    "Description: " + calendarEvent.Description + Environment.NewLine +
                    "Date: " + calendarEvent.Date.ToString("dd.MM.yyyy HH:mm") + Environment.NewLine +
                    "Time left: " + Math.Abs(diff.Days) + " days " + Math.Abs(diff.Hours) + " hours " + Math.Abs(diff.Minutes) + " minutes";

                currentIndex += 1;
            }

            for (int i = 0; i < calendar.SingleEvent.Count; i++)
            {
                CustomRTB textEvent = events[currentIndex + i];

                CalendarEvent calendarEvent = calendar.SingleEvent[i];

                TimeSpan diff = calendarEvent.Date - dateNow;

                if (calendarEvent.Date < dateNow && !calendarEvent.EventReached)
                {
                    Speak(calendarEvent.Description);
                    calendarEvent.EventReached = true;
                }

                if (calendarEvent.EventReached)
                {
                    textEvent.ForeColor = Color.Green;
                }
                else
                {
                    textEvent.ForeColor = Color.DarkGray;
                }

                textEvent.Text =
                    "Description: " + calendarEvent.Description + Environment.NewLine +
                    "Date: " + calendarEvent.Date.ToString("dd.MM.yyyy HH:mm") + Environment.NewLine +
                    "Time left: " + Math.Abs(diff.Days) + " days " + Math.Abs(diff.Hours) + " hours " + Math.Abs(diff.Minutes) + " minutes";
            }

            ResizeEventBoxes();
        }

        public void UpdateDummyEvents()
        {
            int currentIndex = 0;

            for (int i = 0; i < calendar.RecurringEvents.Count; i++)
            {
                CustomRTB textEvent = events[i];

                CalendarEvent calendarEvent = calendar.RecurringEvents[i];

                textEvent.Text = " ";

                currentIndex += 1;
            }

            for (int i = 0; i < calendar.SingleEvent.Count; i++)
            {
                CustomRTB textEvent = events[currentIndex + i];

                CalendarEvent calendarEvent = calendar.SingleEvent[i];

                textEvent.Text = " ";

            }

            ResizeEventBoxes();
        }

        public void UpdateFeed()
        {
            feedWorker.Run();

            ResetFeedBoxes(false);

            int count = 0;
            foreach (Feed.FeedJob job in feedWorker.jobs)
            {
                if (!job.error)
                {
                    CustomRTB textHeader = jobFeeds[count * 2];
                    CustomRTB textData = jobFeeds[count * 2 + 1];

                    textHeader.Tag = true;
                    textData.Tag = true;

                    if (job.isContains)
                    {
                        textHeader.ForeColor = Color.Yellow;
                        textData.ForeColor = Color.Yellow;
                    }
                    else
                    {
                        textHeader.ForeColor = Color.White;
                        textData.ForeColor = Color.White;
                    }

                    textHeader.Text = job.title.Trim();
                    textData.Text = WebUtility.HtmlDecode(job.splitReturn.Trim());

                    textHeader.Update();
                    textData.Update();

                    textHeader.Refresh();
                    textData.Refresh();

                    count++;
                }
            }

            ResizeFeedBoxes();
        }

        public void ResetFeedBoxes(bool setValue)
        {
            int count = 0;
            foreach (Feed.FeedJob job in feedWorker.jobs)
            {
                CustomRTB textHeader = jobFeeds[count * 2];
                CustomRTB textData = jobFeeds[count * 2 + 1];

                textHeader.Tag = setValue;
                textData.Tag = setValue;

                count++;
            }
        }

        public void UpdateDummyFeed()
        {
            int count = 0;
            foreach (Feed.FeedJob job in feedWorker.jobs)
            {
                if (!job.error)
                {
                    CustomRTB textHeader = jobFeeds[count * 2];
                    CustomRTB textData = jobFeeds[count * 2 + 1];

                    textHeader.Text = " ";
                    textData.Text = " ";


                    count++;
                }
            }

            ResizeFeedBoxes();
        }

        private void PlaceCornerImage()
        {
            CornerImage.BackColor = Color.FromArgb(0, 10, 10, 10);
            CornerImage.Location = new Point((this.Width - CornerImage.Width) - 25, 25);
        }

        private void UpdateLayout()
        {
            DateTime time = DateTime.Now;


                lblTime.Text = time.ToString("HH:mm");
                lblTime.Location = GetCenterPosition(lblTime.Size);


            TimeSpan timeNightStart = new TimeSpan(20, 0, 0);
            TimeSpan timeNightEnd = new TimeSpan(7, 0, 0);

            if ((time.TimeOfDay > timeNightStart) || time.TimeOfDay < timeNightEnd)
            {
                if (this.CornerImage.BackgroundImage != global::InfoScreen.Properties.Resources.minimapNight)
                {
                    this.CornerImage.BackgroundImage = global::InfoScreen.Properties.Resources.minimapNight;
                }
            }
            else
            {
                if (this.CornerImage.BackgroundImage != global::InfoScreen.Properties.Resources.minimapDay)
                {
                    this.CornerImage.BackgroundImage = global::InfoScreen.Properties.Resources.minimapDay;
                }
            }

        }

        public void DisplayImage(byte[] byteImage)
        {
            Bitmap bitmap;
            using (var ms = new MemoryStream(byteImage))
            {
                bitmap = (Bitmap)Bitmap.FromStream(ms);
            }

            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(() => {

                    this.BackgroundImageLayout = ImageLayout.Zoom;
                    this.BackgroundImage = bitmap;

                    this.Invalidate();
                    this.Refresh();
                }));
            }
        }

        private Point GetCenterPosition(Size controlSize)
        {
            return new Point((this.Width / 2) - (controlSize.Width / 2), (this.Height / 2) - (controlSize.Height / 2));
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (showGUI)
            {
                UpdateLayout();
                UpdateEvents();
            }
        }

        public void Speak(string text)
        {
            string command = "espeak '" + text + "'";

            Process proc = new Process();
            proc.StartInfo.FileName = "/bin/bash";
            proc.StartInfo.Arguments = "-c \" " + command + " \"";
            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.RedirectStandardOutput = true;
            proc.Start();
        }

        private void timerFeedUpdater_Tick(object sender, EventArgs e)
        {
            timerFeedUpdater.Enabled = false;

            if (TimeLimitHit(lastFeedDate, 30))
            {
                if (showGUI)
                {
                    UpdateFeed();
                }

                lastFeedDate = DateTime.Now;
            }

            timerFeedUpdater.Enabled = true;
        }

        private void timer1_Tick_1(object sender, EventArgs e)
        {
            if (TimeLimitHit(lastWeatherDate, 30))
            {
                if (showGUI)
                {
                    UpdateWeatherBox();
                }

                lastWeatherDate = DateTime.Now;
            }
        }

        public void HideGUI()
        {
            if (showGUI)
            {
                showGUI = false;

                lblTime.Visible = false;
                panelEvents.Visible = false;
                panelFeed.Visible = false;
                imgWeather.Visible = false;
                CornerImage.Visible = false;
                lblTemperature.Visible = false;

                lblCalendar.Visible = false;
                lblFeed.Visible = false;
            }
            else
            {
                showGUI = true;

                lblTime.Visible = true;
                panelEvents.Visible = true;
                panelFeed.Visible = true;
                imgWeather.Visible = true;
                CornerImage.Visible = true;
                lblTemperature.Visible = true;

                lblCalendar.Visible = true;
                lblFeed.Visible = true;
            }
        }

        private void firstRunTimer_Tick(object sender, EventArgs e)
        {
            firstRunTimer.Enabled = false;

            UpdateWeatherBox();
            UpdateEvents();
            UpdateFeed();

            UpdateLayout();
            UpdateEvents();

            timerClock.Enabled = true;
            timerFeedUpdater.Enabled = true;
            weatherTimer.Enabled = true;
        }
    }
}
