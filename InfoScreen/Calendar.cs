﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace InfoScreen
{
    class Calendar
    {
        public List<CalendarEvent> RecurringEvents { get; private set; }
        public List<CalendarEvent> SingleEvent { get; private set; }

        public Calendar()
        {
            this.RecurringEvents = new List<CalendarEvent>();
            this.SingleEvent = new List<CalendarEvent>();

            ReadFile(); 
        }

        private void ReadFile()
        {
            CultureInfo culture = new CultureInfo("no");

            string file = File.ReadAllText("calendar.txt");

            string[] lines = file.Split('\n');

            foreach (string line in lines)
            {
                if (line.ToLower().StartsWith("every"))
                {
                    DateTime nextDate = DateTime.Now;

                    string eventData = line.ToLower().Trim();

                    eventData = eventData.Replace("every ", "");

                    DayOfWeek day = nextDate.DayOfWeek;
                    if (!eventData.Split(' ')[0].Trim().StartsWith("day"))
                    {
                        if (!eventData.Split(' ')[0].Trim().StartsWith("weekday"))
                        {
                            if (day == DayOfWeek.Monday || day == DayOfWeek.Tuesday || day == DayOfWeek.Wednesday ||
                                day == DayOfWeek.Thursday || day == DayOfWeek.Friday)
                            {
                                day = DayOfWeek.Monday;
                            }
                        }
                        else
                        {
                            day = GetDayFromString(eventData.Split(' ')[0].Trim());
                        }
                    }

                    // If this is today then do nothing...
                    if (nextDate.DayOfWeek != day)
                    {
                        while (true)
                        {
                            nextDate = nextDate.AddDays(1);

                            if (nextDate.DayOfWeek == day)
                            {
                                break;
                            }
                        }
                    }

                    string time = eventData.Split(' ')[1].Trim().Split('|')[0].Trim();

                    string description = eventData.Split('|')[1].Trim();

                    DateTime timeDate = DateTime.ParseExact(time, "HH:mm", CultureInfo.InvariantCulture);

                    nextDate = nextDate.Add(-nextDate.TimeOfDay);
                    nextDate = nextDate.Add(timeDate.TimeOfDay);

                    RecurringEvents.Add(new CalendarEvent(nextDate, description));
                }
                else
                if (line.ToLower().StartsWith("at"))
                {
                    string eventData = line.ToLower().Trim();

                    eventData = eventData.Replace("at ", "");

                    string date = eventData.Split(' ')[0].Trim();
                    string time = eventData.Split(' ')[1].Split('|')[0].Trim();
                    string description = eventData.Split('|')[1].Trim();

                    DateTime nextDate = DateTime.ParseExact(date + " " + time, "dd.MM.yyyy HH:mm", CultureInfo.InvariantCulture);

                    SingleEvent.Add(new CalendarEvent(nextDate, description));
                }
            }
        }

        static DayOfWeek GetDayFromString(string v)
        {
            switch (v)
            {
                case "monday":
                    return DayOfWeek.Monday;
                case "tuesday":
                    return DayOfWeek.Tuesday;
                case "wednesday":
                    return DayOfWeek.Wednesday;
                case "thursday":
                    return DayOfWeek.Thursday;
                case "friday":
                    return DayOfWeek.Friday;
                case "saturday":
                    return DayOfWeek.Saturday;
                case "sunday":
                    return DayOfWeek.Sunday;
                default:
                    return DayOfWeek.Monday;
            }
        }
    }
}
