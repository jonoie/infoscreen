﻿using Svg;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace InfoScreen
{
    class Weather
    {
        public List<string> labels = new List<string>();
        private Brush textColor = Brushes.White;
        private int textSizeHour = 17;
        private int textSizeTemp = 15;
        private int textHeaderSize = 35;

        public int GetTemperature()
        {
            try
            {
                WebClient wc = new WebClient();
                wc.Headers.Add("User-Agent: Other");

                string src = wc.DownloadString("http://www.yr.no/sted/Norge/M%C3%B8re_og_Romsdal/%C3%85lesund/%C3%85lesund/varsel.xml");

                string currentTemperature = src.Split(new string[] { "<temperature unit=\"celsius\" value=\"" }, StringSplitOptions.None)[1].
                    Split(new string[] { "\"" }, StringSplitOptions.None)[0];

                return Convert.ToInt32(currentTemperature);
            }
            catch
            {
                return 0;
            }
        }

        public Bitmap GetWeatherBitmap()
        {
            labels.Clear();

            try
            {
                string meteogramUrl = "http://www.yr.no/sted/Norge/M%C3%B8re_og_Romsdal/%C3%85lesund/%C3%85lesund/meteogram.svg";

                WebClient wc = new WebClient();
                wc.Headers.Add("User-Agent: Other");

                string svgFileXML = wc.DownloadString(meteogramUrl);

                svgFileXML = readlXML(svgFileXML);

                var svgDocument = SvgDocument.FromSvg<SvgDocument>(svgFileXML);

                var bitmap = svgDocument.Draw();

                DrawOnBitmap(bitmap);

                return bitmap;
            }
            catch
            {
                Bitmap bmp = new Bitmap(2, 2);

                return bmp;
            }
        }

        public Bitmap DebugGetWeatherBitmap()
        {
            labels.Clear();

            try
            {

                string svgFileXML = File.ReadAllText(@"C:\meteogram.svg"); ;

                svgFileXML = readlXML(svgFileXML);

                var svgDocument = SvgDocument.FromSvg<SvgDocument>(svgFileXML);

                var bitmap = svgDocument.Draw();

                DrawOnBitmap(bitmap);

                return bitmap;
            }
            catch
            {
                Bitmap bmp = new Bitmap(2, 2);

                return bmp;
            }
        }

        public void DrawOnBitmap(Bitmap bm)
        {
            using (Graphics gr = Graphics.FromImage(bm))
            {
                gr.SmoothingMode = SmoothingMode.AntiAlias;
                gr.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gr.PixelOffsetMode = PixelOffsetMode.HighQuality;

                bool skipLine = false;
                bool skipHour = false;
                foreach (string text in labels)
                {
                    string posX = text.Split(new string[] { "x=\"" }, StringSplitOptions.None)[1].Split('"')[0];
                    string posY = text.Split(new string[] { "y=\"" }, StringSplitOptions.None)[1].Split('"')[0];


                    if (text.ToLower().Contains("left-axis"))
                    {
                        if (!skipLine)
                        {
                            string axis = text.Split(new string[] { ">" }, StringSplitOptions.None)[1].Split(new string[] { "</text" }, StringSplitOptions.None)[0];

                            gr.DrawString(axis, new Font("MonoSpace", textSizeTemp, FontStyle.Bold), textColor, new RectangleF(Convert.ToInt32(posX) - 22, Convert.ToInt32(posY) + 16, 0, 0));

                            //   skipLine = true;
                        }
                        else
                        {
                            //  skipLine = false;
                        }
                    }
                    else
                    if (text.ToLower().Contains("right-axis"))
                    {
                        // text-anchor: end
                        //  string axis = text.Split(new string[] { ">" }, StringSplitOptions.None)[1].Split(new string[] { "</text" }, StringSplitOptions.None)[0];

                        //   gr.DrawString(axis, new Font("MonoSpace", textSize, FontStyle.Bold), textColor, new RectangleF(Convert.ToInt32(posX) + 2, Convert.ToInt32(posY) + 16, 0, 0));

                        if (!skipLine)
                        {
                            string axis = text.Split(new string[] { ">" }, StringSplitOptions.None)[1].Split(new string[] { "</text" }, StringSplitOptions.None)[0];

                            gr.DrawString(axis, new Font("MonoSpace", textSizeTemp, FontStyle.Bold), textColor, new RectangleF(Convert.ToInt32(posX), Convert.ToInt32(posY) + 16, 0, 0));

                            skipLine = true;
                        }
                        else
                        {
                            skipLine = false;
                        }
                    }
                    else
                    if (text.ToLower().Contains("hour"))
                    {
                        // text-anchor: middle
                        if (!skipHour)
                        {
                            string hour = text.Split(new string[] { ">" }, StringSplitOptions.None)[1].Split(new string[] { "</text" }, StringSplitOptions.None)[0];

                            gr.DrawString(hour, new Font("MonoSpace", textSizeHour, FontStyle.Bold), textColor, new RectangleF(Convert.ToInt32(posX) - 7, Convert.ToInt32(posY) + 10, 0, 0));

                            skipHour = true;
                        }
                        else
                        {
                            skipHour = false;
                        }
                    }
                    else
                    {
                        if (text.ToLower().Contains("</tspan>") && !text.ToLower().Contains("meteogrammet"))
                        {
                            string dag = text.Split(new string[] { "#505956\">" }, StringSplitOptions.None)[1].Split(new string[] { "</tspan>" }, StringSplitOptions.None)[0];

                            gr.DrawString(dag, new Font("MonoSpace", textHeaderSize, FontStyle.Bold), textColor, new RectangleF(Convert.ToInt32(posX), Convert.ToInt32(posY), 0, 0));

                        }
                    }
                }

                gr.Flush();
            }
        }

        public string readlXML(string src)
        {
            string[] textLines = src.Split(new string[] { "<text x" }, StringSplitOptions.None);
            string[] lines = src.Split('\n');
            foreach (string line in lines)
            {
                if (line.Contains("<path class=\"sun-glow\"") || line.Trim().StartsWith("<line x") || line.Trim().StartsWith("<path fill=\"#8B918F\" ")
                    || line.Trim().Contains("class=\"precip\"") || line.Trim().Contains("class=\"precip-hatch\""))
                {
                    src = src.Replace(line, "");
                }
            }

            for (int i = 1; i < textLines.Length; i++)
            {
                string text = textLines[i].Split(new string[] { "</text>" }, StringSplitOptions.None)[0];

                string textInput = "<text x" + text + "</text>";

                labels.Add(textInput);

                src = src.Replace(textInput, "");
            }

            src = src.Replace("rect.precip-hatch", "rect { \n fill-opacity: 0; \n } \n rect.precip-hatch");
            src = src.Replace("<g transform=\"scale(0.32)\">\r\n              <g transform=\"rotate", "<g transform=\"scale(0.42)\">\r\n              <g transform=\"rotate");
            src = src.Replace("#F01C1C", "#FFFFFF");
            src = src.Replace("fill:black", "fill:lightgrey");
            src = src.Replace("stroke-width=\"2\"", "stroke-width=\"5\"");

            string yrLogo = "<g id=\"logo-yr\">" + src.Split(new string[] { "<g id=\"logo-yr\">" }, StringSplitOptions.None)[1].Split(new string[] { "</g>" }, StringSplitOptions.None)[0] + "</g>";

            src = src.Replace(yrLogo, "");

            return src;
        }
    }
}