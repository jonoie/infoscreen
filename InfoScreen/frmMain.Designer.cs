﻿namespace InfoScreen
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timerClock = new System.Windows.Forms.Timer(this.components);
            this.panelFeed = new System.Windows.Forms.Panel();
            this.lblTime = new System.Windows.Forms.Label();
            this.timerFeedUpdater = new System.Windows.Forms.Timer(this.components);
            this.panelEvents = new System.Windows.Forms.Panel();
            this.weatherTimer = new System.Windows.Forms.Timer(this.components);
            this.lblTemperature = new System.Windows.Forms.Label();
            this.CornerImage = new System.Windows.Forms.PictureBox();
            this.imgWeather = new System.Windows.Forms.PictureBox();
            this.lblFeed = new System.Windows.Forms.Label();
            this.lblCalendar = new System.Windows.Forms.Label();
            this.firstRunTimer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.CornerImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgWeather)).BeginInit();
            this.SuspendLayout();
            // 
            // timerClock
            // 
            this.timerClock.Interval = 35000;
            this.timerClock.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // panelFeed
            // 
            this.panelFeed.BackColor = System.Drawing.Color.Transparent;
            this.panelFeed.Location = new System.Drawing.Point(12, 12);
            this.panelFeed.Name = "panelFeed";
            this.panelFeed.Size = new System.Drawing.Size(608, 855);
            this.panelFeed.TabIndex = 90;
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblTime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 249.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTime.ForeColor = System.Drawing.Color.DimGray;
            this.lblTime.Location = new System.Drawing.Point(215, 152);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(996, 377);
            this.lblTime.TabIndex = 1;
            this.lblTime.Text = "00:00";
            // 
            // timerFeedUpdater
            // 
            this.timerFeedUpdater.Interval = 10000;
            this.timerFeedUpdater.Tick += new System.EventHandler(this.timerFeedUpdater_Tick);
            // 
            // panelEvents
            // 
            this.panelEvents.BackColor = System.Drawing.Color.Transparent;
            this.panelEvents.Location = new System.Drawing.Point(1069, 12);
            this.panelEvents.Name = "panelEvents";
            this.panelEvents.Size = new System.Drawing.Size(608, 855);
            this.panelEvents.TabIndex = 91;
            // 
            // weatherTimer
            // 
            this.weatherTimer.Interval = 120000;
            this.weatherTimer.Tick += new System.EventHandler(this.timer1_Tick_1);
            // 
            // lblTemperature
            // 
            this.lblTemperature.AutoSize = true;
            this.lblTemperature.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblTemperature.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblTemperature.Font = new System.Drawing.Font("Microsoft Sans Serif", 125.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTemperature.ForeColor = System.Drawing.Color.DimGray;
            this.lblTemperature.Location = new System.Drawing.Point(334, 551);
            this.lblTemperature.Name = "lblTemperature";
            this.lblTemperature.Size = new System.Drawing.Size(461, 189);
            this.lblTemperature.TabIndex = 93;
            this.lblTemperature.Text = "-5 °C";
            // 
            // CornerImage
            // 
            this.CornerImage.BackgroundImage = global::InfoScreen.Properties.Resources.minimapDay;
            this.CornerImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.CornerImage.Location = new System.Drawing.Point(743, 45);
            this.CornerImage.Name = "CornerImage";
            this.CornerImage.Size = new System.Drawing.Size(256, 201);
            this.CornerImage.TabIndex = 0;
            this.CornerImage.TabStop = false;
            // 
            // imgWeather
            // 
            this.imgWeather.Location = new System.Drawing.Point(753, 694);
            this.imgWeather.Name = "imgWeather";
            this.imgWeather.Size = new System.Drawing.Size(236, 108);
            this.imgWeather.TabIndex = 92;
            this.imgWeather.TabStop = false;
            // 
            // lblFeed
            // 
            this.lblFeed.AutoSize = true;
            this.lblFeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F);
            this.lblFeed.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.lblFeed.Location = new System.Drawing.Point(626, 31);
            this.lblFeed.Name = "lblFeed";
            this.lblFeed.Size = new System.Drawing.Size(91, 42);
            this.lblFeed.TabIndex = 0;
            this.lblFeed.Text = "feed";
            // 
            // lblCalendar
            // 
            this.lblCalendar.AutoSize = true;
            this.lblCalendar.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCalendar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.lblCalendar.Location = new System.Drawing.Point(820, 592);
            this.lblCalendar.Name = "lblCalendar";
            this.lblCalendar.Size = new System.Drawing.Size(162, 42);
            this.lblCalendar.TabIndex = 94;
            this.lblCalendar.Text = "calendar";
            // 
            // firstRunTimer
            // 
            this.firstRunTimer.Interval = 7000;
            this.firstRunTimer.Tick += new System.EventHandler(this.firstRunTimer_Tick);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Brown;
            this.ClientSize = new System.Drawing.Size(1689, 879);
            this.Controls.Add(this.lblCalendar);
            this.Controls.Add(this.lblFeed);
            this.Controls.Add(this.CornerImage);
            this.Controls.Add(this.lblTemperature);
            this.Controls.Add(this.imgWeather);
            this.Controls.Add(this.lblTime);
            this.Controls.Add(this.panelFeed);
            this.Controls.Add(this.panelEvents);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmMain";
            this.ShowIcon = false;
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.CornerImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgWeather)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timerClock;
        private System.Windows.Forms.Panel panelFeed;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Timer timerFeedUpdater;
        private System.Windows.Forms.Panel panelEvents;
        private System.Windows.Forms.PictureBox imgWeather;
        private System.Windows.Forms.Timer weatherTimer;
        private System.Windows.Forms.Label lblTemperature;
        private System.Windows.Forms.PictureBox CornerImage;
        private System.Windows.Forms.Label lblFeed;
        private System.Windows.Forms.Label lblCalendar;
        private System.Windows.Forms.Timer firstRunTimer;
    }
}

