﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InfoScreen
{
    class CalendarEvent
    {
        public DateTime Date { get; private set; }
        public string Description { get; private set; }
        public bool EventReached { get; set; }

        public CalendarEvent(DateTime Date, string Description)
        {
            this.Date = Date;
            this.Description = Description;

            if (Date > DateTime.Now)
            {
                EventReached = false;
            }
            else
            {
                EventReached = true;
            }
        }
    }
}
