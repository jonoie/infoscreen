﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InfoScreen;

using NetworkCommsDotNet;
using NetworkCommsDotNet.Connections;

using NetworkData;
using NetworkData.Packets;

namespace InfoScreen.Network.PacketHandling
{
    class ReceiveImage : PacketHandler
    {
        public ReceiveImage(Server server) : base(server) { }

        public override void InitializePacket()
        {
            NetworkComms.AppendGlobalIncomingPacketHandler<Image>(PacketNames.sendImage.ToString(), Receive);
        }

        protected void Receive(PacketHeader header, Connection connection, Image incomingImage)
        {
            frmMain.WINDOW.DisplayImage(incomingImage.image);
        }
    }
}
