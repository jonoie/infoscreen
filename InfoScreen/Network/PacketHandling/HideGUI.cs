﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InfoScreen;

using NetworkCommsDotNet;
using NetworkCommsDotNet.Connections;

using NetworkData;
using NetworkData.Packets;

namespace InfoScreen.Network.PacketHandling
{
    class HideGUI : PacketHandler
    {
        public HideGUI(Server server) : base(server) { }

        public override void InitializePacket()
        {
            NetworkComms.AppendGlobalIncomingPacketHandler<int>(PacketNames.hideGUI.ToString(), Receive);
        }

        protected void Receive(PacketHeader header, Connection connection, int number)
        {
            frmMain.WINDOW.HideGUI();
        }
    }
}
