﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net;
using NetworkCommsDotNet;
using NetworkCommsDotNet.DPSBase;
using NetworkCommsDotNet.Connections;
using NetworkCommsDotNet.Tools;
using NetworkCommsDotNet.Connections.TCP;

using InfoScreen.Network.PacketHandling;

namespace InfoScreen.Network
{
    class Server
    {
        private int port = 2918;

        private PacketHandler receiveImage, hideGUI;

        public Server()
        {
            receiveImage = new ReceiveImage(this);
            hideGUI = new HideGUI(this);
            
            NetworkComms.DefaultSendReceiveOptions = new SendReceiveOptions(DPSManager.GetDataSerializer<ProtobufSerializer>(), NetworkComms.DefaultSendReceiveOptions.DataProcessors, NetworkComms.DefaultSendReceiveOptions.Options);

            NetworkComms.Shutdown();
            Connection.StartListening(ConnectionType.TCP, new IPEndPoint(IPAddress.Any, port));
        }
    }
}
