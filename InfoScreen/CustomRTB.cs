﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace InfoScreen
{
    public partial class CustomRTB : RichTextBox
    {
        public CustomRTB()
        {
            InitializeComponent();

            SetStyle(ControlStyles.AllPaintingInWmPaint |
                ControlStyles.SupportsTransparentBackColor, true);
            base.BackColor = Color.FromArgb(120, 10, 10, 10);

            this.TextChanged += CustomRTB_TextChanged;
        }

        private void CustomRTB_TextChanged(object sender, EventArgs e)
        {
            this.Height = (this.GetLineFromCharIndex(this.Text.Length) + 1) * this.Font.Height + 1 + this.Margin.Vertical;
            this.SelectionStart = 0;
            this.SelectionStart = this.Text.Length;
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams CP = base.CreateParams;
                CP.ExStyle |= 0x20;
                return CP;
            }
        }
    }
}
