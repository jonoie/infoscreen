﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.SQLite;
using System.Net;

using System.Threading;
using System.Diagnostics;
using System.IO;

namespace InfoScreen.Feed
{
    class FeedWorker
    {
        private SQLiteConnection dbConnection;
        public List<FeedJob> jobs { get; private set; }
        private int splitMaxLength = 40;

        public FeedWorker()
        {
            jobs = new List<FeedJob>();

            OpenDatabase();
            LoadJobs();
        }

        private void OpenDatabase()
        {
            dbConnection = new SQLiteConnection("Data Source=data.db;Version=3;");
            dbConnection.Open();
        }

        private void LoadJobs()
        {
            SQLiteDataReader reader = dbRead("SELECT * FROM entries WHERE active='y'");
            while (reader.Read())
            {
                jobs.Add(new FeedJob(
                    reader["title"].ToString(),
                    reader["address"].ToString(),
                    System.Web.HttpUtility.UrlDecode(reader["contains"].ToString()),
                    System.Web.HttpUtility.UrlDecode(reader["delim1"].ToString()),
                    System.Web.HttpUtility.UrlDecode(reader["delim2"].ToString()),
                    Convert.ToInt32(reader["splitrows"].ToString()),
                    reader["checkinsplit"].ToString()));
            }
        }

        public void Run()
        {
            WebClient wc = new WebClient();
            wc.Headers.Add("User-Agent: Other");

            foreach (FeedJob job in jobs)
            {
                System.Threading.Thread.Sleep(500);

                // wc.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
                //       wc.Encoding = Encoding.UTF8;

                // fix escape sequences...
                job.contains = job.contains.Replace(@"\n", "\n");
                job.delim1 = job.delim1.Replace(@"\n", "\n");
                job.delim2 = job.delim2.Replace(@"\n", "\n");
                job.splitReturn = "";
                job.error = false;

                try
                {
                    string src = wc.DownloadString(job.address);

                    if (job.splitCheck)
                    {
                        if (job.splitrows == 1)
                        {
                            job.splitReturn = SafeSubstring(src.Split(new string[] { job.delim1 }, StringSplitOptions.None)[1].Split
                                (new string[] { job.delim2 }, StringSplitOptions.None)[0], 0, splitMaxLength);
                        }

                        if (job.splitrows > 1)
                        {
                            for (int i = 0; i < job.splitrows; i++)
                            {
                                job.splitReturn += SafeSubstring(System.Web.HttpUtility.HtmlDecode(src.Split(new string[] { job.delim1 }, StringSplitOptions.None)[i + 1].Split
                                (new string[] { job.delim2 }, StringSplitOptions.None)[0]), 0, splitMaxLength) + Environment.NewLine;
                            }
                        }
                    }
                    
                    if (job.containmentCheck)
                    {
                        if (job.checkinsplit == "y")
                        {
                            if (job.splitReturn.Contains(job.contains))
                            {
                                job.isContains = true;
                            }
                            else
                            {
                                job.isContains = false;
                            }
                        }
                        else
                        {
                            if (src.Contains(job.contains))
                            {
                                job.isContains = true;
                            }
                            else
                            {
                                job.isContains = false;
                            }
                        }
                    }
                }
                catch
                {
                    job.error = true;
                }
            }
        }

        public string SafeSubstring(string text, int start, int length)
        {
            return text.Length <= start ? ""
                : text.Length - start <= length ? text.Substring(start)
                : text.Substring(start, length);
        }

        private SQLiteDataReader dbRead(string sql)
        {
            SQLiteCommand command = new SQLiteCommand(sql, dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            return reader;
        }
    }
}
