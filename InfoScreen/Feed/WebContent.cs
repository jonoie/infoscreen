﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InfoScreen.Feed
{
    class WebContent
    {
        public string title { get; set; }
        public string address { get; set; }
        public string status { get; set; }
        public string elapsed { get; set; }

        public WebContent(string Title, string Address, string Status, string Elapsed)
        {
            title = Title;
            address = Address;
            status = Status;
            elapsed = Elapsed;
        }
    }
}
