﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InfoScreen.Feed
{
    class FeedJob
    {
        public bool containmentCheck = false;
        public bool splitCheck = false;

        public string title { get; set; }
        public string address { get; set; }
        public string contains { get; set; }
        public string delim1 { get; set; }
        public string delim2 { get; set; }
        public string checkinsplit { get; set; }
        public int splitrows { get; set; }

        // Results from thread...
        public string splitReturn { get; set; }
        public bool isContains { get; set; }
        public bool error { get; set; }

        public FeedJob(string Title, string Address, string Contains, string Delim1, string Delim2, int Splitrows, string Checkinsplit)
        {
            if (Delim1.Length != 0 | Delim2.Length != 0)
            {
                splitCheck = true;
            }

            if (Contains.Length != 0)
            {
                containmentCheck = true;
            }

            title = Title;
            address = Address;
            contains = Contains;
            delim1 = Delim1;
            delim2 = Delim2;
            splitrows = Splitrows;
            checkinsplit = Checkinsplit;

            splitReturn = "";
        }
    }
}
